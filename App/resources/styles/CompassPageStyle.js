import {StyleSheet} from 'react-native';
import colors from '../colors';
const CompassPageStyle = StyleSheet.create({
  container: {
    backgroundColor: colors.colorBackground,
    flex: 1,
  },
  row: {flexDirection: 'row'},
  viewFlex: {flex: 1},
  textHeader: {
    color: colors.black,
    fontSize: 20,
    alignSelf: 'baseline',
    backgroundColor: 'rgba(45,45,255,0.2)',
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
    marginStart: 10,
  },
  textLocationHistory: {
    color: colors.black,
    fontSize: 20,
    alignSelf: 'baseline',
    backgroundColor: 'rgba(45,45,255,0.2)',
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
    marginEnd: 10,
  },
  textLatitude: {color: colors.black, fontSize: 20, padding: 10, flex: 1},
  textLongitude: {color: colors.black, fontSize: 20, padding: 10, flex: 1},
  textDirection: {color: colors.black, fontSize: 14, padding: 10, flex: 1},
});

export default CompassPageStyle;
