const colors = {
  white: '#FFFFFF',
  black: '#000000',
  colorBackground: '#FFFFFF',
  colorTransparent: 'transparent',
};

export default colors;
