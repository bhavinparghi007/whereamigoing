import * as ActionType from './ActionTypes';

export const doSetLocationHistory = data => {
  return dispatch => {
    dispatch({type: ActionType.LOCATION_HISTORY, payload: data});
  };
};

export const doAddLocation = data => {
  return dispatch => {
    dispatch({type: ActionType.ADD_LOCATION, payload: data});
  };
};

export const doSetCurrentLocation = data => {
  return dispatch => {
    dispatch({type: ActionType.CURRENT_LOCATION, payload: data});
  };
};
