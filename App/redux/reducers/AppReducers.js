import * as ActionType from '../actions/ActionTypes';

const INITIAL_STATE = {
  currentLatLong: {
    latitude: 0,
    longitude: 0,
  },
  latLongHistory: [],
};

const AppReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ActionType.CURRENT_LOCATION:
      return {
        ...state,
        currentLatLong: action.payload,
      };
    case ActionType.ADD_LOCATION:
      return {
        ...state,
        latLongHistory: [...state.latLongHistory, action.payload],
      };
    case ActionType.LOCATION_HISTORY:
      return {
        ...state,
        latLongHistory: action.payload,
      };
    default:
      return state;
  }
};

export default AppReducer;
