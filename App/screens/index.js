import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import CompassPage from './CompassPage';
import LocationHistoryPage from './LocationHistoryPage';
const AppStack = createStackNavigator(
  {
    CompassPage: {
      screen: CompassPage,
      navigationOptions: {header: null},
    },
    LocationHistory: {
      screen: LocationHistoryPage,
      navigationOptions: {header: null},
    },
  },
  {
    initialRouteName: 'CompassPage',
  },
);

const AppIndex = createAppContainer(AppStack);
export default AppIndex;
