import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import React, {PureComponent} from 'react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TextInput,
  Image,
} from 'react-native';
import {
  doSetCurrentLocation,
  doAddLocation,
  doSetLocationHistory,
} from '../redux/actions/AppAction';
import LocationListItem from '../components/list/LocationListItem';
import LocationsPageStyle from '../resources/styles/LocationsPageStyle';
import labels from '../resources/labels';
import colors from '../resources/colors';
import icons from '../resources/icons';
class LocationHistoryPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      latLongHistory: props.latLongHistory,
      search: '',
    };
  }
  componentDidMount() {
    
  }
  onSearchText = text => {
    clearTimeout(this.searchTimeout);

    this.setState({search: text, loading: true}, () => {
      var filtered = this.props.latLongHistory.filter((value, index) => {
        return value.direction
          .toLowerCase()
          .includes(this.state.search.toLowerCase());
      });
      if (this.state.search.length <= 0) {
        this.setState({
          latLongHistory: this.props.latLongHistory,
          loading: false,
        });
        return;
      }
      this.setState({latLongHistory: filtered, loading: false});
    });
  };
  renderEmptyContainer = () => {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Image source={icons.LOCATION_LIST} style={{width: 80, height: 80}} />
        <Text style={{color: colors.black, padding: 10, textAlign: 'center'}}>
          {labels.NO_LOCATION_HISTORY}
        </Text>
      </View>
    );
  };
  renderSeparator = () => {
    return <View style={LocationsPageStyle.viewSeprator} />;
  };
  render() {
    return (
      <View style={LocationsPageStyle.container}>
        <Text style={LocationsPageStyle.textHeader}>
          {labels.LOCATION_HISTORY}
        </Text>
        <View style={LocationsPageStyle.viewSearch}>
          <TextInput
            placeholder={labels.SEARCH}
            underlineColorAndroid={colors.colorTransparent}
            style={LocationsPageStyle.inputSearch}
            value={this.state.search}
            onChangeText={text => this.onSearchText(text)}
          />
          <View style={LocationsPageStyle.viewLoading}>
            {this.state.loading ? (
              <ActivityIndicator color={colors.black} />
            ) : (
              undefined
            )}
          </View>
        </View>
        <FlatList
          data={this.state.latLongHistory}
          renderItem={({item}) => (
            <LocationListItem
              latitude={item.latitude.toFixed(4)}
              longitude={item.longitude.toFixed(4)}
              direction={item.direction}
              date={item.dateLabel}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={this.renderSeparator}
          ListEmptyComponent={this.renderEmptyContainer}
          contentContainerStyle={
            this.state.latLongHistory.length <= 0 && {height: '100%'}
          }
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentLatLong: state.app.currentLatLong,
    latLongHistory: state.app.latLongHistory,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {doSetLocationHistory, doAddLocation, doSetCurrentLocation},
      dispatch,
    ),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LocationHistoryPage);
